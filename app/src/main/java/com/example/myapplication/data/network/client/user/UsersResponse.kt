package com.example.myapplication.data.network.client.user

import com.fasterxml.jackson.annotation.JsonProperty

class UsersResponse (
    @JsonProperty("users")
    val users:List<UserNetworkEntity>)