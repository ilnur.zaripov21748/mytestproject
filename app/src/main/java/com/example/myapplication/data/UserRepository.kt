package com.example.myapplication.data

import android.content.Context
import com.example.myapplication.data.dataBase.AppDatabase
import com.example.myapplication.data.dataBase.entity.UserTable
import com.example.myapplication.data.network.UserApi
import com.example.myapplication.data.network.NetworkManager
import com.example.myapplication.ui.fragment.user.list.adapter.User
import io.reactivex.Single
import java.util.*

class UserRepository(val context: Context) : IUserRepository {

    private var db: AppDatabase? = null

    init {
        db = AppDatabase.getAppDataBase(context)
    }

    override fun getUsers(): Single<List<User>> {
        val s1 = getUsersFromDb()
        val s2 = getUsersFromNet()
            .doOnSuccess { it ->
                db?.userDao()?.insertUsers(userNetEntityToUserTable(it)) }

        return s2
        /*Single.concat(s1,s2)
            .filter { it.isNotEmpty() }
            .toList() as Single<List<User>>*/
    }

    private fun userNetEntityToUserTable(it: List<User>?): ArrayList<UserTable> {
        val arrayList:ArrayList<UserTable> = ArrayList()
        if (it != null) {
            for (element in it){
                arrayList.add(UserTable(element.id,
                element.name,
                element.surname,
                element.countSubscribers,
                element.countFollowers,
                element.countLikes,
                element.countReach,
                element.countComments,
                element.srcAvatar))
            }
        }
        return arrayList
    }

    private fun getUsersFromNet(): Single<List<User>> {
            val api = NetworkManager.getRetrofit()!!.create(UserApi::class.java)
            return api.getUsers()
                .map { it.users }
                .toObservable()
                .flatMapIterable { it }
                .map { User(it.id,
                    it.name,
                    it.lastname,
                    it.followers,
                    it.followers,
                    it.likes,
                    it.reach,
                    it.comments,
                    "") }
                .toList()
    }

    private fun getUsersFromDb(): Single<List<User>> {
        return db?.userDao()?.getAllUsers()
            ?.toObservable()
            ?.flatMapIterable { it }
            ?.map {
                User(
                    it.id,
                    it.name,
                    it.surname,
                    it.countSubscribers,
                    it.countFollowers,
                    it.countLikes,
                    it.countReach,
                    it.countComments,
                    ""
                )
            }
            ?.toList() as Single<List<User>>
    }

    override fun getUsersByName(name: String): Single<List<User>> {
        return getUsers()
            .toObservable()
            .flatMapIterable { it }
            .filter { it.name.contains(name,true) || it.surname.contains(name,true)}
            .toList()
    }

    override fun getUserById(userId: Int): Single<User> {
        return getUsers()
            .toObservable()
            .flatMapIterable { it }
            .filter { it.id == userId }
            .singleOrError()
    }
}