package com.example.myapplication.data.dataBase.dao

import androidx.room.*
import com.example.myapplication.data.dataBase.entity.UserTable
import io.reactivex.Single

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: UserTable)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsers(users: ArrayList<UserTable>)

    @Update
    fun updateUser(user: UserTable)

    @Delete
    fun deleteUser(user: UserTable)

    @Query("SELECT * FROM UserTable WHERE name like '%'||:name||'%' or surname like '%'||:name||'%'")
    fun getUsers(name: String): Single<List<UserTable>>

    @Query("SELECT * FROM UserTable")
    fun getAllUsers(): Single<List<UserTable>>
}