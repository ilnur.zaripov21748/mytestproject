package com.example.myapplication.data

import android.content.Context
import com.example.myapplication.data.dataBase.AppDatabase
import com.example.myapplication.ui.fragment.user.list.adapter.User
import io.reactivex.Single

class UserRepositoryDb(val context: Context) : IUserRepository {

    private var db: AppDatabase? = null

    init {
        db = AppDatabase.getAppDataBase(context)
    }

    override fun getUsers(): Single<List<User>> {
        return db?.userDao()?.getAllUsers()
            ?.toObservable()
            ?.flatMapIterable { it }
            ?.map { User(it.id,
                it.name,
                it.surname,
                it.countSubscribers,
                it.countFollowers,
                it.countLikes,
                it.countReach,
                it.countComments,
                "") }
            ?.toList() as Single<List<User>>
    }

    override fun getUsersByName(name: String): Single<List<User>> {
        return getUsers()
            .toObservable()
            .flatMapIterable { it }
            .filter { it.name.contains(name,true) || it.surname.contains(name,true)}
            .toList()
    }

    override fun getUserById(userId: Int): Single<User> {
        return getUsers()
            .toObservable()
            .flatMapIterable { it }
            .filter { it.id == userId }
            .singleOrError()
    }
}