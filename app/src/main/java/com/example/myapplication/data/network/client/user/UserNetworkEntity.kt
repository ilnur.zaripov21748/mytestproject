package com.example.myapplication.data.network.client.user

import com.fasterxml.jackson.annotation.JsonProperty

class UserNetworkEntity (
    @JsonProperty("id")
    val id: Int,
    @JsonProperty("name")
    val name: String,
    @JsonProperty("lastname")
    val lastname: String,
    @JsonProperty("likes")
    val likes: Int,
    @JsonProperty("followers")
    val followers: Int,
    @JsonProperty("reach")
    val reach: Int,
    @JsonProperty("comments")
    val comments: Int)
