package com.example.myapplication.data.dataBase.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserTable(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    val id: Int,
    val name: String,
    val surname: String,
    val countSubscribers: Int,
    val countFollowers: Int,
    val countLikes: Int,
    val countReach: Int,
    val countComments:Int,
    val srcAvatar:String?)