package com.example.myapplication.data.network

import com.example.myapplication.data.network.client.user.UsersResponse
import io.reactivex.Single
import retrofit2.http.GET

interface UserApi {

    @GET("/iZIVer/test-api/db")
    fun getUsers(): Single<UsersResponse>
}