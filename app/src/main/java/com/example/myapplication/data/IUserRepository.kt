package com.example.myapplication.data

import com.example.myapplication.ui.fragment.user.list.adapter.User
import io.reactivex.Single

interface IUserRepository {

    fun getUsers() : Single<List<User>>

    fun getUsersByName(name: String) : Single<List<User>>

    fun getUserById(userId: Int) : Single<User>
}