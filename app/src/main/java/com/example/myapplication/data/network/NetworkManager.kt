package com.example.myapplication.data.network

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory


class NetworkManager {
    companion object{
        var INSTANCE: Retrofit? = null

        fun getRetrofit(): Retrofit? {
            if (INSTANCE == null){
                synchronized(NetworkManager::class){
                    INSTANCE = Retrofit.Builder()
                            .baseUrl("https://my-json-server.typicode.com")
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .addConverterFactory(JacksonConverterFactory.create(getObjectMapper()))
                            .client(getOkHttpClient())
                            .build()
                }
            }
            return INSTANCE
        }

        private fun getObjectMapper(): ObjectMapper? {
            val objectMapper = ObjectMapper()
            objectMapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, true)
            objectMapper.configure(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS, true)
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
            return objectMapper
        }

        private fun getOkHttpClient(): OkHttpClient? {
            return OkHttpClient.Builder().build()
        }

        fun destroyRetrofit(){
            INSTANCE = null
        }
    }
}