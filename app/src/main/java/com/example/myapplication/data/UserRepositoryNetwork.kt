package com.example.myapplication.data

import com.example.myapplication.ui.fragment.user.list.adapter.User
import com.example.myapplication.data.network.UserApi
import com.example.myapplication.data.network.NetworkManager
import io.reactivex.Observable
import io.reactivex.Single

class UserRepositoryNetwork : IUserRepository {


    fun getUsersFromNetwork(query: String): Observable<List<User>> {
        val api = NetworkManager.getRetrofit()!!.create(UserApi::class.java)
        return api.getUsers()
            .map { it.users }
            .toObservable()
            .flatMapIterable { it }
//            .map { UserTable(it.id, it.name, it.lastname, it.followers, it.followers, it.likes, it.reach, it.comments, null) }
            .map { User(it.id, it.name, it.lastname, it.followers, it.followers, it.likes, it.reach, it.comments, "") }
            .toList()
            .toObservable()
    }

    fun getUserFromNetwork(userId: Int): Observable<User> {
        val api = NetworkManager.getRetrofit()!!.create(UserApi::class.java)
        return api.getUsers()
            .map { it.users }
            .toObservable()
            .flatMapIterable { it }
            .filter { it.id == userId }
            .map { User(it.id, it.name, it.lastname, it.followers, it.followers, it.likes, it.reach, it.comments, "") }
    }

    override fun getUsers(): Single<List<User>> {
        val api = NetworkManager.getRetrofit()!!.create(UserApi::class.java)
        return api.getUsers()
            .map { it.users }
            .toObservable()
            .flatMapIterable { it }
            .map { User(it.id, it.name, it.lastname, it.followers, it.followers, it.likes, it.reach, it.comments, "") }
            .toList()
    }

    override fun getUsersByName(name: String): Single<List<User>> {
        return getUsers()
            .toObservable()
            .flatMapIterable { it }
            .filter { it.name.contains(name) }
            .toList()
    }

    override fun getUserById(userId: Int): Single<User> {
        return getUsers()
            .toObservable()
            .flatMapIterable { it }
            .filter { it.id == userId }
            .singleOrError()
    }
}