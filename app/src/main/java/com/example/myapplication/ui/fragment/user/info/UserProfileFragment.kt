package com.example.myapplication.ui.fragment.user.info

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.myapplication.R
import com.example.myapplication.data.IUserRepository
import com.example.myapplication.data.UserRepository
import com.example.myapplication.ui.fragment.user.list.adapter.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class UserProfileFragment : Fragment() {

    private lateinit var userListData: IUserRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userListData = UserRepository(requireContext())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_profile, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
    }

    private fun setupUI() {
        val view = view
//        val user = requireArguments().getSerializable("user") as User
        val userId = requireArguments().getInt("user_id")
        userListData.getUserById(userId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ it -> onGetUserSuccess(it!!) }, { it -> onGetUserError(it) })



    }

    private fun onGetUserSuccess(user: User) {
        val view = requireView()

        view.findViewById<TextView>(R.id.profileName).text = user.name+" "+user.surname
        view.findViewById<TextView>(R.id.profileCountSub).text = user.countSubscribers.toString() + " subscribers"
        view.findViewById<TextView>(R.id.followerCount).text = user.countFollowers.toString()
        view.findViewById<TextView>(R.id.likeCount).text = user.countLikes.toString()
        view.findViewById<TextView>(R.id.reachCount).text = user.countReach.toString()
        view.findViewById<TextView>(R.id.commentCount).text = user.countComments.toString()
        view.findViewById<ImageView>(R.id.profileAvatar).setImageURI(Uri.parse("srcAvatar"))
    }

    private fun onGetUserError(error: Throwable) {
        Log.e("Error", error.message.toString())
        Toast.makeText(context, error.message, Toast.LENGTH_SHORT)
    }
}