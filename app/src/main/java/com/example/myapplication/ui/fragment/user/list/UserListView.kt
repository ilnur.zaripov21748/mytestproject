package com.example.myapplication.ui.fragment.user.list

import com.example.myapplication.ui.fragment.user.list.adapter.User

interface UserListView {

    fun onSearchUsersSuccess(users: List<User>)

    fun onSearchUsersError(error: Throwable)
}