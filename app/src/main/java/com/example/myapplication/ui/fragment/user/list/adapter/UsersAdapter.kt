package com.example.myapplication.ui.fragment.user.list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R


class UsersAdapter(val listener: OnUserClickListener) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {
    var users: List<User> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(users[position], listener)
    }


    override fun getItemCount() = users.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val fio: TextView = itemView.findViewById(R.id.userListName)
        val avatar: ImageView = itemView.findViewById(R.id.userListAvatar)
        val countSubscriber: TextView = itemView.findViewById(R.id.userListCountSub)

        fun bind(user: User, listener: OnUserClickListener) {

            fio.text = user.name + " " + user.surname
            countSubscriber.text = user.countSubscribers.toString() + " subscribers"

            itemView.setOnClickListener {
                listener.OnUserClick(user)
            }
        }

    }
}
