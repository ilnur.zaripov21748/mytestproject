package com.example.myapplication.ui.fragment.user.list

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.ui.fragment.user.list.adapter.OnUserClickListener
import com.example.myapplication.ui.fragment.user.list.adapter.User
import com.example.myapplication.ui.fragment.user.list.adapter.UsersAdapter
import com.example.myapplication.rx.search.RxSearchViewBuilder

class UserListFragment : Fragment(), UserListView {

    private var presenter: UserListPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = UserListPresenter(requireContext(), this)
    }

    private var adapter: UsersAdapter = UsersAdapter(object : OnUserClickListener {
        override fun OnUserClick(user: User) {
            val arg = Bundle()
//            arg.putSerializable("user", user)
            arg.putInt("user_id", user.id)
            findNavController().navigate(R.id.userProfileDest, arg)
        }
    })

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_user_list, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
        val rx = RxSearchViewBuilder.buildObservable(menu)
            .filter { isQueryApprove(it) }
        presenter?.getUsersSubscribe(rx)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun isQueryApprove(it: String) = it.length >= 3 || it.isEmpty()

    fun showProgress() {
        val loadPB = view?.findViewById<ProgressBar>(R.id.loadDataProgress)
        loadPB?.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        val loadPB = view?.findViewById<ProgressBar>(R.id.loadDataProgress)
        loadPB?.visibility = View.GONE
    }

    override fun onSearchUsersSuccess(users: List<User>) {
        Toast.makeText(context, "Поиск завершен", Toast.LENGTH_SHORT).show()
        adapter.users = users!!

        adapter.notifyDataSetChanged()
        hideProgress()
    }

    override fun onSearchUsersError(error: Throwable) {
        Log.e("Error", error.message.toString())
        Toast.makeText(context, error.message, Toast.LENGTH_SHORT)
        hideProgress()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewListUsers = view?.findViewById<RecyclerView>(R.id.viewListUsers)
        viewListUsers?.layoutManager = LinearLayoutManager(context)

        viewListUsers.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter = null
    }
}