package com.example.myapplication.ui.fragment.user.list

import android.content.Context
import android.util.Log
import com.example.myapplication.data.IUserRepository
import com.example.myapplication.data.UserRepository
import com.example.myapplication.data.UserRepositoryDb
import com.example.myapplication.ui.fragment.user.list.adapter.User
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class UserListPresenter(private val context: Context, private val view: UserListView) {

    private var userListData: IUserRepository

    init {
        userListData = UserRepository(context)
    }

    fun getUsersSubscribe(rx: Observable<String>) {
        rx
            .observeOn(Schedulers.io())
            .flatMap { search -> userListData.getUsersByName(search).toObservable() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ it -> onSearchUsersSuccess(it!!) }, { it -> onSearchUsersError(it) })
    }

    private fun onSearchUsersSuccess(users: List<User>) {
        view.onSearchUsersSuccess(users)
    }

    private fun onSearchUsersError(error: Throwable) {
        Log.e("Error", error.message.toString())
        view.onSearchUsersError(error)
    }
}