package com.example.myapplication.ui.fragment.user.list.adapter

import java.io.Serializable

class User(val id: Int,
           val name: String,
           val surname: String,
           val countSubscribers: Int,
           val countFollowers: Int,
           val countLikes: Int,
           val countReach: Int,
           val countComments:Int,
           val srcAvatar:String) : Serializable