package com.example.myapplication

import android.app.Application
import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.myapplication.data.dataBase.AppDatabase
import com.example.myapplication.data.dataBase.entity.UserTable

class AppDelegate : Application(){
    override fun onCreate() {
        super.onCreate()
        initDataBase(this)
    }
    var INSTANCE: AppDatabase? = null

    private fun initDataBase(context: Context): AppDatabase? {
        if (AppDatabase.INSTANCE == null){
            synchronized(AppDatabase::class){

                AppDatabase.INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "myDB")
                        .addCallback(object: RoomDatabase.Callback() {

                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)
                                val users: ArrayList<UserTable> = ArrayList()

                               /* users.add(UserTable(null,"Name", "Ser",1000,2231,3234,412,563,"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0JJvLFd5cfR_kZecQL3KxqbrEo1uRZNPKVw&usqp=CAU"))
                                users.add(UserTable(null,"Bla", "Bla",5234,234,123,3,234,"https://img.icons8.com/material/4ac144/256/user-male.png"))
                                users.add(UserTable(null,"Sidor","Sidorobich",123,63456,6784,2345,456,"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTjfbJWV0m8JVmZzIUHCriJFVuS1ee73XeTw&usqp=CAU"))
                                users.add(UserTable(null,"Igor", "Nickolaevich",34,3452,2345,7546,2,"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQkMzRm0aoFKoW2hYxHGK6i6syJLukqEy8Ang&usqp=CAU"))
*/
                                AppDatabase.INSTANCE?.userDao()?.insertUsers(users)
                                INSTANCE = AppDatabase.INSTANCE
                            }
                        }).build()
            }
        }
        return AppDatabase.INSTANCE
    }
}