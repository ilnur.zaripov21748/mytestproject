package com.example.myapplication.rx.search;

import android.annotation.SuppressLint;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;


import io.reactivex.Observable;

import static androidx.core.util.Preconditions.checkNotNull;

public class RxSearchView {

    @SuppressLint("RestrictedApi")
    @CheckResult
    @NonNull
    public static Observable<SearchViewQueryTextEvent> queryTextChangeEvents(@NonNull SearchView view) {
        checkNotNull(view,"view == null");
        return Observable.create(new SearchViewQueryTextChangesOnSubscribe(view));
    }
}
