package com.example.myapplication.rx.search;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;

import com.example.myapplication.rx.ViewEvent;

import java.util.Objects;


public final class SearchViewQueryTextEvent extends ViewEvent<SearchView> {

    @CheckResult
    @NonNull
    public static SearchViewQueryTextEvent create(@NonNull SearchView view,
                                                  @NonNull CharSequence queryText, boolean submitted) {
        return new SearchViewQueryTextEvent(view, queryText, submitted);
    }

    private final CharSequence mQueryText;
    private final boolean mSubmitted;

    private SearchViewQueryTextEvent(@NonNull SearchView view, @NonNull CharSequence queryText,
                                     boolean submitted) {
        mQueryText = queryText;
        mSubmitted = submitted;
    }

    @NonNull
    public CharSequence queryText() {
        return mQueryText;
    }

    public boolean isSubmitted() {
        return mSubmitted;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchViewQueryTextEvent that = (SearchViewQueryTextEvent) o;
        return mSubmitted == that.mSubmitted &&
                mQueryText.equals(that.mQueryText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mQueryText, mSubmitted);
    }
}
