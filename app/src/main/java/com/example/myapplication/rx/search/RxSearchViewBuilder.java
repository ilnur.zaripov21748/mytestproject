package com.example.myapplication.rx.search;

import android.annotation.SuppressLint;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;


import com.example.myapplication.R;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Конструктор слушателя поисковой строки
 */
public class RxSearchViewBuilder {

    /**
     * @param menu - опциональное меню окна
     * @return - Observable изменения строки поиска
     */
    @NonNull
    public static Observable<String> buildObservable(@NonNull Menu menu) {
        return buildObservable(menu, "Search");
    }

    /**
     * @param menu - опциональное меню окна
     * @param hint - текст посказка для поля ввода
     * @return - Observable изменения строки поиска
     */
    @SuppressLint("ResourceAsColor")
    @NonNull
    public static Observable<String> buildObservable(@NonNull Menu menu, @NonNull String hint) {
        MenuItem searchMenuItem = menu.findItem(R.id.app_bar_search);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();
        SearchView.SearchAutoComplete textView = searchView.findViewById(R.id.search_src_text);

        textView.setTextSize(16);
        searchView.setQueryHint(hint);


        return RxSearchView.queryTextChangeEvents(searchView)
                .debounce(200, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .map(e -> e.queryText().toString());
    }
}
