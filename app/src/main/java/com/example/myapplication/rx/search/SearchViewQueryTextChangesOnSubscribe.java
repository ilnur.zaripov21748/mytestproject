package com.example.myapplication.rx.search;

import androidx.appcompat.widget.SearchView;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

final class SearchViewQueryTextChangesOnSubscribe implements ObservableOnSubscribe<SearchViewQueryTextEvent> {

    private final SearchView mView;

    SearchViewQueryTextChangesOnSubscribe(SearchView view) {
        this.mView = view;
    }

    @Override
    public void subscribe(ObservableEmitter<SearchViewQueryTextEvent> emitter) {
        SearchView.OnQueryTextListener watcher = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!emitter.isDisposed()) {
                    emitter.onNext(SearchViewQueryTextEvent.create(mView, query, false));
                    return true;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!emitter.isDisposed()) {
                    emitter.onNext(SearchViewQueryTextEvent.create(mView, mView.getQuery(), true));
                    return true;
                }
                return false;
            }
        };
        mView.setOnQueryTextListener(watcher);

        emitter.onNext(SearchViewQueryTextEvent.create(mView, mView.getQuery(), false));
    }
}
